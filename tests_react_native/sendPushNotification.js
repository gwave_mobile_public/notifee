var admin = require('firebase-admin');
var serviceAccount = require('./notifee-admin.json');

// TODO: put as env
const TOKEN = ['d_l_Wcr2IUQbuwBU7DhQUt:APA91bGhLHBUkrtFdnA9seFSs74MegcGkFWGky6tt1oT8eaS_tW8UUfKZ3uhbIwdkbufrioF_H2fOc5fF1ZvsLtof1_oL-A1BBU2LFmyJcZiBmnLyABIncXbCJtNuXoURm2A0cUoDk6-',
  ]

var payload = {
  android: {
    priority: 'high',
  },
  notification: {
    body: 'A notification body111',
    title: 'A notification title!',
  },
  apns: {
    payload: {
      aps: {
        sound: 'default',
        category: 'communications',
        mutableContent: 1,
        contentAvailable: 1,
      },
      notifee_options: {
        image: 'https://placeimg.com/640/480/any', // URL to pointing to a remote image
        ios: {
          communicationInfo: {
            conversationId: 'id-abcde',
            sender: {
              id: 'senderId',
              avatar: 'https://placeimg.com/640/480/any',
              displayName: 'Helena Ford',
            },
          },
        },
      },
    },
  },
  tokens: TOKEN,
};
admin.initializeApp({
  credential: admin.credential.cert(serviceAccount)
});
admin
  .messaging()
  .sendMulticast(payload)
  .then(response => {
    console.log('Successfully sent message:', response);
    console.log(response.responses[0])
  })
  .catch(error => {
    console.log('Error sending message:', error);
  })
  .finally(() => process.exit());
